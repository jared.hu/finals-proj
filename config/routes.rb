Rails.application.routes.draw do
    #https://github.com/plataformatec/devise/wiki/How-To:-Define-a-different-root-route-for-logged-in-out-users
    devise_for :cashiers, only: [:sessions]
    devise_for :admins, only: [:sessions]
    resources :products
    resources :orders
    resources :cashiers
    post 'orders/summary', to: "orders#summary"
    
    root 'publics#index'
    get "/products/:id", to: "products#deactivate"
    # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

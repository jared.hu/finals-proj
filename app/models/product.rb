class Product < ApplicationRecord
    default_scope { where(status: true) }
    belongs_to :admin
    has_many :order_lines
end

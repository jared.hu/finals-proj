class OrdersController < ApplicationController
    #before_action :authenticate_cashier! 
    
    def new
        @order = Order.new
    end
    
    def summary
        raise order_params.inspect
        @order = Order.new(order_params)
        raise @order.order_lines[0].product.price.inspect
        i=0
        @order.order_lines.each do |o|
            i += o.product.price * o.quantity
        end
        @order.total_price = i
        
    end
    
    def create
        @order = Order.new(order_params)
        @order.cashier = current_cashier
        @order.save
    
        redirect_to @order
    end
    
    def index
        @orders = Order.all
    end
    
    def edit
        @order = Order.find(params[:id])
        if @order.cashier != current_cashier
            redirect_to orders_path
        end
    end
    
    def update
        @order = Order.find(params[:id])
        @order.update(order_params)
        @order.save
        
        redirect_to order_path
    end
    
    def destroy
        @order = Order.find(params[:id])
        @order.destroy
        
        redirect_to orders_path
    end

    def show
        id = params[:id]
        @order = Order.find(id)
    end
    
    
    private
        def order_params
            params.require(:order).permit!
        end
end

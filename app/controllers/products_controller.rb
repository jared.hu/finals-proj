class ProductsController < ApplicationController
    # before_action :authenticate_admin! 
    def new
        @product = Product.new
    end
    def create
        @product = Product.new(product_params)
        @product.admin = current_admin
        @product.save
        
        redirect_to @product
    end
    def show
        @product = Product.find(params[:id])
        
    end
    def index
        @products = Product.all
    end
    def edit
        @product = Product.find(params[:id])
        if @product.admin != current_admin
            @product = Product.find(params[:id])
        elsif @product.admin == nil
            @product = Product.find(params[:id])
        end
    end
    def update
        @product = Product.find(params[:id])
        @product.update(product_params)
        @product.save
        redirect_to @product
    end
    
    def destroy
        @product = Product.find(params[:id])
        @product.destroy
        redirect_to @product
    end
    
    def deactivate
        @product = Product.find(params[:id])
        @product.status = false
    end
    
    
    private
        def product_params
            params.require(:product).permit(:title, :price, :status)
        end
end

class CashiersController < ApplicationController
    # before_action :authenticate_admin! 
    def new
        @cashier = Cashier.new
    end
    def create
        @cashier = Cashier.new(cashier_params)
        @cashier.admin = current_admin
        @cashier.inspect
        @cashier.save
        
        redirect_to @cashier
    end
    def show
        @cashier = Cashier.find(params[:id])
        if @cashier.admin != current_admin
            redirect_to cashiers_path
        elsif @cashier.admin == nil
            redirect_to cashiers_path
        end
        
    end
    def index
        @cashiers = Cashier.all
    end
    def edit
        @cashier = Cashier.find(params[:id])
    end
    def update
        @cashier = Cashier.find(params[:id])
        @cashier.update(product_params)
        @cashier.save
        redirect_to cashiers_path
    end
    def destroy
        @cashier = Cashier.find(params[:id])
        @cashier.destroy
        redirect_to cashiers_path
    end
    
    private
        def cashier_params
            params.require(:cashier).permit!
        end
end



class ApplicationController < ActionController::Base
    protect_from_forgery with: :exception
    # https://github.com/plataformatec/devise/wiki/How-To:-redirect-to-a-specific-page-on-successful-sign-in
    def after_sign_in_path_for(resource)
        if resource.is_a?(Admin)
          products_path
        elsif resource.is_a?(Cashier)
          orders_path 
        end
    end
    
end
